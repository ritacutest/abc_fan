﻿using UnityEngine;
using UnityEditor;
using System.Collections;

///Developed by Indie Studio
///https://www.assetstore.unity3d.com/en/#!/publisher/9268
///www.indiestd.com
///info@indiestd.com
///copyright © 2016 IGS. All rights reserved.

[CustomEditor(typeof(ShapesManager))]
public class ShapesManagerEditor : Editor
{
		private Color greenColor = Color.green;
		private Color whiteColor = Color.white;
		private Color redColor = new Color (255, 0, 0, 255) / 255.0f;

		public override void OnInspectorGUI ()
		{
				ShapesManager shapesManager = (ShapesManager)target;//get the target

				EditorGUILayout.Separator ();

				GUILayout.BeginHorizontal ();
				if (GUILayout.Button ("Review Alphabet Tracing", GUILayout.Width (180), GUILayout.Height (25))) {
						Application.OpenURL ("");
				}

				GUI.backgroundColor = greenColor;         

				if (GUILayout.Button ("More Assets", GUILayout.Width (110), GUILayout.Height (25))) {
						Application.OpenURL ("https://www.assetstore.unity3d.com/en/#!/search/page=1/sortby=popularity/query=publisher:9268");
				}
				GUI.backgroundColor = whiteColor;         

				GUILayout.EndHorizontal ();

				EditorGUILayout.Separator ();
	


				GUILayout.BeginHorizontal ();
				GUI.backgroundColor = greenColor;         

				if (GUILayout.Button ("Add New Shape", GUILayout.Width (110), GUILayout.Height (20))) {
						shapesManager.shapes.Add (new ShapesManager.Shape ());
				}

				GUI.backgroundColor = redColor;         
				if (GUILayout.Button ("Remove Last Shape", GUILayout.Width (150), GUILayout.Height (20))) {
						if (shapesManager.shapes.Count != 0) {
								shapesManager.shapes.RemoveAt (shapesManager.shapes.Count - 1);
						}
				}

				GUI.backgroundColor = whiteColor;
				GUILayout.EndHorizontal ();

				EditorGUILayout.Separator ();

				for (int i = 0; i <  shapesManager.shapes.Count; i++) {
						shapesManager.shapes [i].showContents = EditorGUILayout.Foldout (shapesManager.shapes [i].showContents, "Shape[" + i + "]");

						if (shapesManager.shapes [i].showContents) {
								EditorGUILayout.Separator ();
								shapesManager.shapes [i].gamePrefab = EditorGUILayout.ObjectField ("Game Prefab", shapesManager.shapes [i].gamePrefab, typeof(GameObject), true) as GameObject;
								EditorGUILayout.Separator ();
								GUILayout.Box ("", GUILayout.ExpandWidth (true), GUILayout.Height (2));
						}
				}

				if (GUI.changed) {
						DirtyUtil.MarkSceneDirty ();
				}
		}
}
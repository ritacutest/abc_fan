﻿using UnityEngine;
using UnityEditor;
using System.Collections;

///Developed by Indie Studio
///https://www.assetstore.unity3d.com/en/#!/publisher/9268
///www.indiestd.com
///info@indiestd.com
///copyright © 2016 IGS. All rights reserved.

[CustomEditor(typeof(Path))]
public class PathEditor : Editor {

	public override void OnInspectorGUI ()
	{
		Path path = (Path)target;//get the target
		
		EditorGUILayout.Separator ();
		path.fillMethod = (Path.FillMethod)EditorGUILayout.EnumPopup ("Fill Method",path.fillMethod);
		if (path.fillMethod == Path.FillMethod.Linear) {
			path.type = (Path.ShapeType)EditorGUILayout.EnumPopup ("Type",path.type);
			path.offset = EditorGUILayout.Slider ("Offset", path.offset, -360, 360);
			path.flip = EditorGUILayout.Toggle("Flip",path.flip);
		}
		path.completeOffset = EditorGUILayout.Slider ("Complete Offset", path.completeOffset,0,1);
		path.firstNumber = EditorGUILayout.ObjectField ("First Number",path.firstNumber, typeof(Transform)) as Transform;
		path.secondNumber = EditorGUILayout.ObjectField ("Second Number",path.secondNumber, typeof(Transform)) as Transform;

	}
}	
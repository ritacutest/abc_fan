﻿using UnityEngine;
using System.Collections;

///Developed by Indie Studio
///https://www.assetstore.unity3d.com/en/#!/publisher/9268
///www.indiestd.com
///info@indiestd.com
///copyright © 2016 IGS. All rights reserved.
using AudienceNetwork;


/// Escape or Back event
public class EscapeEvent : MonoBehaviour
{
		/// <summary>
		/// The name of the scene to be loaded.
		/// </summary>
		public string sceneName;

		/// <summary>
		/// Whether to leave the application on escape click.
		/// </summary>
		public bool leaveTheApplication;


		//FAN
		private AdView adView;
			
		void Awake(){

			// Create a banner's ad view with a unique placement ID (generate your own on the Facebook app settings).
			// Use different ID for each ad placement in your app.
			AdView adView = new AdView ("1148435185252828_1148477901915223", AdSize.BANNER_HEIGHT_50);
			this.adView = adView;
			this.adView.Register (this.gameObject);

			// Set delegates to get notified on changes or when the user interacts with the ad.
			this.adView.AdViewDidLoad = (delegate() {
				Debug.Log("Ad view loaded.");
				this.adView.Show(AudienceNetwork.Utility.AdUtility.height() - 50);

			});
			adView.AdViewDidFailWithError = (delegate(string error) {
				Debug.Log("Ad view failed to load with error: " + error);
			});
			adView.AdViewWillLogImpression = (delegate() {
				Debug.Log("Ad view logged impression.");
			});
			adView.AdViewDidClick = (delegate() {
				Debug.Log("Ad view clicked.");
			});

			// Initiate a request to load an ad.
			adView.LoadAd ();

		}

		void OnDestroy () {
			// Dispose of banner ad when the scene is destroyed
			if (this.adView) {
				this.adView.Dispose ();
			}
			Debug.Log("AdViewTest was destroyed!");
		}

		void Update ()
		{
				if (Input.GetKeyDown (KeyCode.Escape)) {
						OnEscapeClick ();
				}
		}

		/// <summary>
		/// On Escape click event.
		/// </summary>
		public void OnEscapeClick ()
		{
				if (leaveTheApplication) {
						GameObject exitConfirmDialog = GameObject.Find ("ExitConfirmDialog");
						if (exitConfirmDialog != null) {
								Dialog exitDialogComponent = exitConfirmDialog.GetComponent<Dialog> ();
								if (!exitDialogComponent.animator.GetBool ("On")) {
										exitDialogComponent.Show ();
										//AdsManager.instance.ShowAdvertisment (AdsManager.AdAPI.AdEvent.Event.ON_SHOW_EXIT_DIALOG);
								}
						}
				} else {
						StartCoroutine (SceneLoader.LoadSceneAsync (sceneName));
				}
		}
}
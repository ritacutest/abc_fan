******************************
		English Alphabet Tracing A-Z
		   Version 1.0.0
******************************
Thank you for downloading English Alphabet Tracing A-Z.

If you have any questions, suggestions, comments ,more details, or feature requests, 
you can send email to "info@indiestd.com".

Download Music From : 
http://www.pond5.com/stock-music/49024359/bright-paint.html

Notes : 
- Use the first AudioSource component in AudioSources GameObject for the music.

Website
https://www.indiestd.com

Facebook Page
https://www.facebook.com/IndieGamesStudio

Asset Store
https://www.assetstore.unity3d.com/en/#!/search/page=1/sortby=popularity/query=publisher:9268

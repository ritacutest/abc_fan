﻿using UnityEngine;
using System.Collections;
using Facebook.Unity;

public class AppEventFB : MonoBehaviour {

	private GameObject target;

	void Awake() {

		if (FB.IsInitialized) {
			FB.ActivateApp();
			Debug.Log("FBactive");
		} else {
			//Handle FB.Init
			FB.Init( () => {
				FB.ActivateApp();
				Debug.Log("FBactive2");
			});
		}

	}

}
